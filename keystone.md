# Obama rejects Keystone XL project, citing U.S. climate leadership

hellop

tasdeasdstama announced Friday that his administration will not issue a permit for construction of the controversial Keystone XL pipeline, arguing that approval would undermine the U.S. effort to curb greenhouse gases.

![Obama](http://www.usnews.com/cmsmedia/c4/22/1e9c4e1743619199769861c8ada0/150106-obamakeystone-editorial.jpg)


"America’s now a global leader when it comes to taking serious action to fight climate change,” Obama said. “And frankly, approving this project would have undercut that global leadership. And that’s the biggest risk we face — not acting.”

Denying TransCanada Corp. a permit for the 1,179-mile pipeline between Hardisty, Alberta, and Steele City, Neb., ends a seven-year fight over a project that became a symbol in the political battle over climate change.

Several former administration officials said Friday that Obama and Secretary of State John F. Kerry had decided to block the project two years ago but waited for the legally required internal review, a revised permit application and, finally, a politically opportune time to announce the decision.

Backers of the project said it would ensure a secure supply of oil from a reliable U.S. ally and create jobs; opponents said it would exacerbate climate change by releasing a massive amount of carbon into the atmosphere and would produce pollution hazards along the pipeline’s route.

Sveral former administration officials said Friday that Obama and Secretary of State John F. Kerry had decided to block the project two years ago but waited for the legally required internal review, a revised permit application and, finally, a politically opportune time to announce the decision.

Backers of the project said it would ensure a secure supply of oil from a reliable U.S. ally and create jobs; opponents said it would exacerbate climate change by releasing a massive amount of carbon into the atmosphere and would produce pollution hazards along the pipeline’s route.

![pipeline](http://blogs.cas.suffolk.edu/connormulcahy/files/2014/03/keystone-xl-map.jpg)

What started as a routine permit application for a project to move 830,000 barrels of crude oil a day to Gulf Coast refineries became a political litmus test for Obama, who said Friday that the pipeline had taken on “an overinflated role in our political discourse.”

The State Department, which controls permits for projects that cross international boundaries, concluded that neither approval nor rejection of the project would significantly alter the globe’s overall carbon output or U.S. gasoline prices.

"America’s now a global leader when it comes to taking serious action to fight climate change,” Obama said. “And frankly, approving this project would have undercut that global leadership. And that’s the biggest risk we face — not acting.”

Denying TransCanada Corp. a permit for the 1,179-mile pipeline between Hardisty, Alberta, and Steele City, Neb., ends a seven-year fight over a project that became a symbol in the political battle over climate change.